## Getting started

1- Replace `YOUR_BASE_FOLDER` in the `nginx.conf` file by the full bath of your backend

2- Put your certificates inside the `certs` folder:
*  `privkey.pem`: The private key. Keep this file private and secure at all times. Compromise of this file (for example, emailing this file, downloaded by a third party etc.) can allow third parties to hijack your SSL certificate.
*  `chain.pem`: The certificate chain (root certificate and all intermediate certificates) needed to validate the certificate is signed by a trusted root certificate.
*  `fullchain.pem`: A combination of both cert.pem and fullchain.pem.

3- Run the command: `nginx -p PATH_TO_THIS_FOLDER -tc nginx.conf`. This command will test if your configuration is valid or not. If it is valid, remove the `t` flag to start the server.
